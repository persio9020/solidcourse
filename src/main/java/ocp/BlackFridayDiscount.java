package ocp;

/**
 *
 * @author persi
 */
public class BlackFridayDiscount{

  public Double apply(Double price) {
    return price * 0.4;
  }
}
