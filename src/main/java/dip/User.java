package dip;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author persi
 */
@Getter
@Setter
public class User {
  private int id;
  private String name;
  private String email;

  User(int id, String name, String email) {
    this.id = id;
    this.name = name;
    this.email = email;
  }
  
}
