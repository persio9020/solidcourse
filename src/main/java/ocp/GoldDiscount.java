package ocp;

/**
 *
 * @author persi
 */
public class GoldDiscount{

  public Double apply(Double price) {
    return price * 0.3;
  }
}
