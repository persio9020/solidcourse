package srp;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author persi
 */
@Getter
@Setter
public class Player {

  int id;
  String name;
  int win;
  int lose;
  String date;

  /**
   *
   * Capa de persistencia
   */
  void savePlayer(Player player) {
    //Salva el jugador
  }

  void deletePlayer(Player player) {
    //Borrar el jugador
  }

  /**
   * Capa de negocio
   */
  Double winPercentage(Player player) {
    return Double.valueOf(player.getWin()/(player.getWin()+player.getLose()));
  }

  Double losePercentage(Player player) {
    return Double.valueOf(player.getLose()/(player.getLose()+player.getWin()));
  }

  /**
   * Capa de presentación
   */
  void showPlayerHD(Player player) {

  }

  void showPlayer(Player player) {

  }
}
