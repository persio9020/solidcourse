package dip;

/**
 *
 * @author persi Sirve para mostrar en pantalla
 */
public class UserManager {

  public void showUser() {
    UserRepository userRepository = new UserRepository();
    User user = userRepository.getUser();
    System.out.println(user.getEmail() + " : " + user.getName());
  }
}
