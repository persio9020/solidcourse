package isp;

/**
 *
 * @author persi
 */
public interface Payment {

  void calculatePayment();

  void creditCardPayment();
  
  void bankTransferPayment();
  
  void cashPayment();
}
