package lsp;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author persi
 * Pista
 */
@Getter
@Setter
public class Court {
  private int width;
  private int height;
  private String grass;
  
  public void cutGrass(){
    
  }
}
