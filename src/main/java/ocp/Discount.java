package ocp;

/**
 *
 * @author persi
 */
public class Discount {

  public Double apply(Double price){
    return price * 0.5;
  }
}
